@extends('admin.layouts.app') @section('content')
<body class="">
  <div class="page">
    <div class="page-single">
      <div class="container">
        <div class="row">
          <div class="col col-login mx-auto">
            <div class="text-center mb-6">
              <img src="{{ $logo_url }}" class="h-8" alt="" />
            </div>
            <form class="card" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="card-body p-6">
                <div class="card-title">{{ __("Create New Account") }}</div>
                <div class="form-group">
                  <label class="form-label">{{ __("Name") }}</label>
                  <input
                    type="text"
                    name="name"
                    class="form-control"
                    placeholder="{{ __("Enter name") }}"
                  />
                  @if ($errors->has('name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label class="form-label">{{ __("E-Mail Address") }}</label>
                  <input
                    type="email"
                    name="email"
                    class="form-control"
                    placeholder="{{ __("Enter email") }}"
                  />
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label class="form-label">{{ __("Password") }}</label>
                  <input
                    type="password"
                    name="password"
                    class="form-control"
                    placeholder="{{ __("Password") }}"
                  />
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label class="form-label">{{ __("Confirm Password") }}</label>
                  <input
                    type="password"
                    name="password_confirmation"
                    class="form-control"
                    placeholder="{{ __("Confirm Password") }}"
                  />
                </div>
                <div class="form-group">
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" />
                    <span class="custom-control-label"
                      >{{ __("Agree the ")
                      }}<a href="#">{{ __("terms and policy") }}</a></span
                    >
                  </label>
                </div>
                <div class="form-footer">
                  <button type="submit" class="btn btn-primary btn-block">
                    {{ __("Create New Account") }}
                  </button>
                </div>
              </div>
            </form>
            <div class="text-center text-muted">
              {{ __("Already have account?") }}
              <a href="{{ route('admin.login') }}">{{ __("Sign In") }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
@endsection
