@extends('admin.layouts.app')

@section('content')
<body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                  <img src="{{ $logo_url }}" class="h-8" alt="" />
              </div>
              @if (session('status'))
              <div class="alert alert-success">
                {{ session("status") }}
              </div>
              @endif
              <form class="card" method="POST" action="{{ route('password.request') }}" >
              <!-- Need csrf hidden token named '_token' -->
              <!-- and hidden token from email named 'token' -->
              {{ csrf_field() }}
              <input type="hidden" name="token" value="{{ $token }}">
                <div class="card-body p-6">
                  <p class="text-muted">
                    {{ __( "Enter your email and new password for the account." ) }}
                  </p>
                  <div class="form-group">
                    <label class="form-label" for="exampleInputEmail1">{{
                      __("E-Mail Address")
                    }}</label>
                    <input
                    id="email"
                    type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email"
                    aria-describedby="emailHelp"
                    placeholder="{{ __("Enter email") }}"
                  />
                   @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      {{ __("New password") }}
                    </label>
                    <input
                      id="password"
                      type="password"
                      class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                      name="password"
                      placeholder="{{ __("Enter password") }}"
                    />
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>    
                  <div class="form-group">
                    <label class="form-label">{{ __("Confirm new password") }}</label>
                    <input
                      type="password"
                      name="password_confirmation"
                      class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                      placeholder="{{ __("Confirm new password") }}"
                    />
                    @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif                    
                  </div>                                
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">
                      {{ __("Reset Password") }}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
@endsection
