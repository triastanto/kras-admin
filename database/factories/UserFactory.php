<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'personnel_no' => $faker->numberBetween(1000, 9000),
        'name' => $faker->name,
        'esgrp' => $faker->randomElement(['A','B','C', 'D', 'E', 'F']),
        'cost_ctr' => $faker->numberBetween(110000, 119999),
        'position_name' => $faker->sentence(3),
        'org_unit_name' => $faker->sentence(4),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
