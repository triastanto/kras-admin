<?php

namespace App\Providers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // URL to logo
        View::composer('*', function ($view) {
            $view->with('logo_url', Storage::url('logo.png'));
        });
        // Using class based composers...
        View::composer(
            'admin.layouts.header', 'App\Http\ViewComposers\Admin\HeaderLayoutComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
