<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HeaderLayoutComposer
{
    /**
     * The authenticated user
     * 
     * @var App\User
     */
    protected $authenticatedUser;

    /**
     * Create a new App Layout composer.
     *
     * @param void
     * @return void
     */
    public function __construct()
    {
        $this->authenticatedUser = Auth::user();
    }

    /**
     * Bind data to the view
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('user', $this->authenticatedUser);
    }
}
