@extends('layouts.app') @section('content')

<body class="">
  <div class="page">
    <div class="page-single">
      <div class="container">
        <div class="row">
          <div class="col col-login mx-auto">
            <div class="text-center mb-6">
              <img src="{{ $logo_url }}" class="h-8" alt="" />
            </div>
            <form class="card" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              <div class="card-body p-6">
                <div class="card-title">
                  {{ __("Login") }}
                </div>
                <div class="form-group">
                  <label class="form-label">{{ __("E-Mail Address") }}</label>
                  <input
                    id="email"
                    type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email"
                    aria-describedby="emailHelp"
                    placeholder="{{ __("Enter email") }}"
                  />
                  @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label class="form-label">
                    {{ __("Password") }}
                    <a
                      href="{{ route('password.request') }}"
                      class="float-right small"
                      >{{ __("Forgot Your Password?") }}</a
                    >
                  </label>
                  <input
                    id="password"
                    type="password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password"
                    placeholder="{{ __("Enter password") }}"
                  />
                  @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input"
                    name="remember" id="remember"
                    {{ old("remember") ? "checked" : "" }}/>
                    <span class="custom-control-label">{{
                      __("Remember Me")
                    }}</span>
                  </label>
                </div>
                <div class="form-footer">
                  <button type="submit" class="btn btn-primary btn-block">
                    {{ __("Login") }}
                  </button>
                </div>
              </div>
            </form>
            @if (isset($register))
            <div class="text-center text-muted">
              {{ __("Don't have account yet?") }}
              <a href="{{ route('register') }}">{{ __("Register") }}</a>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
@endsection
