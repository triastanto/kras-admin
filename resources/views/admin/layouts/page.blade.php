@extends('admin.layouts.app') @section('content')
<body class="">
  <div class="page">
    <div class="page-main">
      <!-- Begin of Header -->
      @include('admin.layouts.header')
      <!-- End of Header -->

      <!-- Begin of Menu Bar -->
      @include('admin.layouts.menubar')
      <!-- End of Menu Bar -->

      <!-- Begin of Page Content -->
      @yield('pagecontent')
      <!-- End of Page Content -->
    </div>
    <!-- Begin of Footer -->
    @include('admin.layouts.footer')
    <!-- End of Footer -->
  </div>
</body>
@endsection
