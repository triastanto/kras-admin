@extends('admin.layouts.page') @section('pagecontent')

<!-- Begin of Dashboard-->
<div class="my-3 my-md-5">
  <div class="container">
    <!-- Begin of Page Title -->
    <div class="page-header">
      <h1 class="page-title">
        Dashboard
      </h1>
    </div>
    <!-- End of Page Title -->

    <!-- Begin of Lens -->
    @include('admin.dashboard.lens')
    <!-- End of Lens -->

    <!-- Begin of Charts -->
    @include('admin.dashboard.chart')
    <!-- End of Charts -->

    <!-- Begin of Summary table -->
    @include('admin.dashboard.summary')
    <!-- End of Summary table -->
  </div>
</div>
<!-- End of Dashboard -->

@endsection
