@extends('admin.layouts.app') @section('content')
<body class="">
  <div class="page">
    <div class="page-single">
      <div class="container">
        <div class="row">
          <div class="col col-login mx-auto">
            <div class="text-center mb-6">
                <img src="{{ $logo_url }}" class="h-8" alt="" />
            </div>
            @if (session('status'))
            <div class="alert alert-success">
              {{ session("status") }}
            </div>
            @endif
            <form
              class="card"
              method="POST"
              action="{{ route('password.email') }}"
            >
              {{ csrf_field() }}
              <div class="card-body p-6">
                <div class="card-title">{{ __("Forgot password") }}</div>
                <p class="text-muted">
                  {{
                    __(
                      "Enter your email address and your password will be reset and emailed to you."
                    )
                  }}
                </p>
                <div class="form-group">
                  <label class="form-label" for="exampleInputEmail1">{{
                    __("E-Mail Address")
                  }}</label>
                  <input
                  id="email"
                  type="email"
                  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                  name="email"
                  aria-describedby="emailHelp"
                  placeholder="{{ __("Enter email") }}"
                />
                 @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-footer">
                  <button type="submit" class="btn btn-primary btn-block">
                    {{ __("Send me new password") }}
                  </button>
                </div>
              </div>
            </form>
            <div class="text-center text-muted">
              <a href="{{ route('admin.login') }}">{{ __("Send me back") }}</a
              >{{ __(" to the sign in screen.") }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
@endsection
